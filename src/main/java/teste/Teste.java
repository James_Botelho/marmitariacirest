package teste;

import br.ufpb.ci.marmitaria.model.dao.FornecedorDAO;
import br.ufpb.ci.marmitaria.model.domain.Cliente;
import br.ufpb.ci.marmitaria.model.domain.Fornecedor;

import java.util.Date;

public class Teste {
    public static void main(String[] args) throws InterruptedException {
        Fornecedor f = new Fornecedor();
        f.setUsuario("james_");
        f.setEndereco("Rua Carrapateira, 126");
        f.setNomeEmpresa("Almoço do James");
        f.setTelefone("83999154081");
        f.setSenha("0987654321");
        Cliente cliente = new Cliente(0, "james_", "123", "12345", "James de Olivera");
        System.out.println(cliente.toJson());
    }
}
