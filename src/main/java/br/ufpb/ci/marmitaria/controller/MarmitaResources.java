package br.ufpb.ci.marmitaria.controller;

import br.ufpb.ci.marmitaria.controller.seguranca.Seguro;
import br.ufpb.ci.marmitaria.model.business.GerenciaMarmitas;
import br.ufpb.ci.marmitaria.utils.excecao.SemAutorizacaoException;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("marmitas")
public class MarmitaResources {

    @Seguro //Verificar se é cliente
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscaTodasAsMarmitas(@Context SecurityContext securityContext){
        String listaCardapio;
        try {
            listaCardapio = GerenciaMarmitas.listaTodosCardapios(securityContext.getUserPrincipal().getName());
        } catch (SemAutorizacaoException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.ok(listaCardapio).build();
    }

    @Seguro //Verificar se é fornecedor
    @Path("{id}") //id do fornecedor
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscaTodasMarmitasPorIdDoFornecedor(@PathParam("id") int id, @Context SecurityContext securityContext){
        String listaCardapio;
        try {
            listaCardapio = GerenciaMarmitas.listaCardapioDoFornecedor(id, securityContext.getUserPrincipal().getName());
        } catch (SemAutorizacaoException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.ok(listaCardapio).build();
    }

    @Seguro //Verificar se é fornecedor
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response adicionaMarmita(String conteudo, @Context SecurityContext securityContext){
        try {
            GerenciaMarmitas.adicionaMarmita(conteudo, securityContext.getUserPrincipal().getName());
        } catch (SemAutorizacaoException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.ok().build();
    }

    @Seguro //Verificar se é fornecedor
    @Path("{id}")
    @DELETE
    public Response removeMarmita(@PathParam("id") int idCardapio, @Context SecurityContext securityContext){
        try {
            GerenciaMarmitas.removeMarmita(idCardapio, securityContext.getUserPrincipal().getName());
        } catch (SemAutorizacaoException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.ok().build();
    }
}
