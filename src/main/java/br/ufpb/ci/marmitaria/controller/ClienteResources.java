package br.ufpb.ci.marmitaria.controller;

import br.ufpb.ci.marmitaria.controller.seguranca.Seguro;
import br.ufpb.ci.marmitaria.model.business.GerenciaUsuario;
import br.ufpb.ci.marmitaria.utils.excecao.LoginExistenteException;
import br.ufpb.ci.marmitaria.utils.excecao.SemAutorizacaoException;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.security.NoSuchAlgorithmException;

@Path("cliente")
public class ClienteResources {

    @Seguro //Verificar se é fornecedor
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String buscaTodosOsClientes(){
        return null;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response cadastraCliente(String conteudo){
        try {
            GerenciaUsuario.insereCliente(conteudo);
        } catch (LoginExistenteException e) {
            return Response.status(Response.Status.CONFLICT).build();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return Response.ok().build();
    }

    @Seguro
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response atualizaCliente(String conteudo, @Context SecurityContext securityContext){
        try {
            GerenciaUsuario.atualizaCliente(conteudo, securityContext.getUserPrincipal().getName());
        } catch (SemAutorizacaoException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.ok().build();
    }

    @Seguro
    @Path("{id}")
    @DELETE
    public Response deletaCliente(@PathParam("id") int idCliente, @Context SecurityContext securityContext){
        try {
            GerenciaUsuario.removeCliente(idCliente, securityContext.getUserPrincipal().getName());
        } catch (SemAutorizacaoException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.ok().build();
    }

    @Seguro
    @Path("{id}/pedidos")
    public Response getPedidosCliente(@PathParam("id") int idCliente, @Context SecurityContext securityContext){
        return null;
    }
}
