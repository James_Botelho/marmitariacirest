package br.ufpb.ci.marmitaria.controller;

import br.ufpb.ci.marmitaria.controller.seguranca.Seguro;
import br.ufpb.ci.marmitaria.model.business.GerenciaUsuario;
import br.ufpb.ci.marmitaria.utils.excecao.LoginExistenteException;
import br.ufpb.ci.marmitaria.utils.excecao.SemAutorizacaoException;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.security.NoSuchAlgorithmException;

@Path("fornecedor")
public class FornecedorResources {

    @Seguro //Verificar se é cliente
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscaTodosFornecedores(@Context SecurityContext securityContext){
        String listaFornecedor;
        try {
            listaFornecedor = GerenciaUsuario.listaTodosFornecedores(securityContext.getUserPrincipal().getName());
        } catch (SemAutorizacaoException e) {
            System.out.println("Sem autorizacao");
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.ok(listaFornecedor).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response cadastraFornecedor(String conteudo){
        try{
            GerenciaUsuario.insereFornecedor(conteudo);
        }catch (LoginExistenteException e){
            return Response.status(Response.Status.CONFLICT).build();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return Response.ok().build();
    }

    @Seguro
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response atualizaFornecedor(String conteudo, @Context SecurityContext securityContext){
        try{
            GerenciaUsuario.atualizaFornecedor(conteudo, securityContext.getUserPrincipal().getName());
        }catch(SemAutorizacaoException e){
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.ok().build();
    }

    @Seguro
    @Path("{id}")
    @DELETE
    public Response deletaFornecedor(@PathParam("id") int idFornecedor, @Context SecurityContext securityContext){
        try{
            GerenciaUsuario.removeFornecedor(idFornecedor, securityContext.getUserPrincipal().getName());
        }catch(SemAutorizacaoException e){
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.ok().build();
    }

    @Seguro
    @Path("{id}/pedidos")
    @GET
    public Response getPedidosFornecedor(@PathParam("id") int idFornecedor, @Context SecurityContext securityContext){
        return null;
    }
}
