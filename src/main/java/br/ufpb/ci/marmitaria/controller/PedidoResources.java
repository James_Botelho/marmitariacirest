package br.ufpb.ci.marmitaria.controller;

import br.ufpb.ci.marmitaria.controller.seguranca.Seguro;
import br.ufpb.ci.marmitaria.model.business.GerenciaPedido;
import br.ufpb.ci.marmitaria.utils.excecao.SemAutorizacaoException;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Seguro
@Path("pedido")
public class PedidoResources {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscaPedidos( @Context SecurityContext securityContext){
        String listacardapio;
        try{
            listacardapio = GerenciaPedido.listaPedido(securityContext.getUserPrincipal().getName());
        }catch (SemAutorizacaoException e){
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.ok(listacardapio).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response adicionaPedido(String conteudo, @Context SecurityContext securityContext){

        try {
            GerenciaPedido.adicionaPedido(conteudo, securityContext.getUserPrincipal().getName());
        } catch (SemAutorizacaoException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    public Response deletaPedido(@PathParam("id") int id, @Context SecurityContext securityContext){

        try{
            GerenciaPedido.deletaPedido(id, securityContext.getUserPrincipal().getName());
        }catch (SemAutorizacaoException e){
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        return Response.ok().build();
    }
}
