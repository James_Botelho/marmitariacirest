package br.ufpb.ci.marmitaria.controller;

import br.ufpb.ci.marmitaria.model.business.GerenciaLogin;
import br.ufpb.ci.marmitaria.model.domain.Login;
import br.ufpb.ci.marmitaria.utils.excecao.LoginIncorretoException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.security.NoSuchAlgorithmException;

@Path("login")
public class LoginResources {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response logaUsuario(String conteudo){
        String retorno = null;
        try {
            retorno = GerenciaLogin.realizaLogin(conteudo);
        } catch (LoginIncorretoException e) {
            //Retorna 401 caso as credenciais informadas estejam incorretas
            return Response.status(Response.Status.UNAUTHORIZED).build();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return Response.ok(retorno).build();
    }
}
