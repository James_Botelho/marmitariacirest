package br.ufpb.ci.marmitaria.controller.seguranca;

import io.jsonwebtoken.Claims;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.security.Principal;

@Seguro
@Provider
@Priority(Priorities.AUTHENTICATION)
public class FiltroAutenticacao implements ContainerRequestFilter {

    private void modificaRequestContext(ContainerRequestContext context, String id){
        final SecurityContext currentSecurityContext = context.getSecurityContext();
        context.setSecurityContext(new SecurityContext() {
            @Override
            public Principal getUserPrincipal() {
                return new Principal() {
                    @Override
                    public String getName() {
                        return id;
                    }
                };
            }

            @Override
            public boolean isUserInRole(String s) {
                return true;
            }

            @Override
            public boolean isSecure() {
                return currentSecurityContext.isSecure();
            }

            @Override
            public String getAuthenticationScheme() {
                return "Bearer";
            }
        });
    }


    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        String header = containerRequestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        if(header == null || !header.startsWith("Bearer ")){
            throw new NotAuthorizedException("É necessário estar autenticado!");
        }

        String token = header.substring("Bearer".length()).trim();

        try {
            Claims claims = GerenciaToken.validaToken(token);
            if(claims == null)
                throw new Exception("Token inválido");

            modificaRequestContext(containerRequestContext, claims.getIssuer());
        }catch (Exception e){
            e.printStackTrace();
            containerRequestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        }
    }
}
