package br.ufpb.ci.marmitaria.controller.seguranca;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.util.Date;

public class GerenciaToken {
    private static final String PALAVRA_CHAVE = "CHAVED23EAC432ESSO324SECRETA";

    public static String geraToken(int id){
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS512;

        byte[] palavraChaveBytes = DatatypeConverter.parseBase64Binary(PALAVRA_CHAVE);

        SecretKeySpec chave = new SecretKeySpec(palavraChaveBytes, signatureAlgorithm.getJcaName());

        JwtBuilder construtor = Jwts.builder()
                .setIssuedAt(new Date())
                .setIssuer(String.valueOf(id))
                .signWith(signatureAlgorithm, chave);

        return construtor.compact();
    }

    public static Claims validaToken(String token){
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(PALAVRA_CHAVE))
                .parseClaimsJws(token).getBody();
        return claims;
    }
}
