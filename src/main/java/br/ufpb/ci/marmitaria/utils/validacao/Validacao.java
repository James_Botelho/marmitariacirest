package br.ufpb.ci.marmitaria.utils.validacao;

public interface Validacao<T> {
    void valida(T t);
}
