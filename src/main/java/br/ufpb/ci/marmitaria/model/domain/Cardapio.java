package br.ufpb.ci.marmitaria.model.domain;

import com.google.gson.Gson;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "cardapio")
public class Cardapio implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String itens;
    private int qtdItens;
    @Temporal(TemporalType.DATE)
    private Date data;
    private String preco;
    private int idFornecedor;

    public Cardapio(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItens() {
        return itens;
    }

    public void setItens(String itens) {
        this.itens = itens;
    }

    public int getQtdItens() {
        return qtdItens;
    }

    public void setQtdItens(int qtdItens) {
        this.qtdItens = qtdItens;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public int getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public String toJson(){
        return new Gson().toJson(this);
    }
}
