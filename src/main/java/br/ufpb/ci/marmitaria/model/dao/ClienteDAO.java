package br.ufpb.ci.marmitaria.model.dao;

import br.ufpb.ci.marmitaria.model.domain.Cliente;
import br.ufpb.ci.marmitaria.utils.JpaUtil;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class ClienteDAO {

    public static void salvaCliente(Cliente cliente){
        EntityManager entityManager = JpaUtil.getEntityManager();
        try{
            entityManager.getTransaction().begin();
            entityManager.persist(cliente);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            if(entityManager.isOpen())
                entityManager.getTransaction().rollback();
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
    }

    public static void atualizaCliente(Cliente cliente){
        EntityManager entityManager = JpaUtil.getEntityManager();
        try{
            entityManager.getTransaction().begin();
            entityManager.merge(cliente);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            if(entityManager.isOpen())
                entityManager.getTransaction().rollback();
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
    }

    public static Cliente recuperaClientePorId(int id){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String consulta = "select new br.ufpb.ci.marmitaria.model.domain.Cliente(u.id, u.usuario, u.senha, u.telefone, c.nomeUsuario) from usuario u inner join cliente c on u.id = c.id where u.id = :id";
        Cliente cliente;
        try {
            TypedQuery<Cliente> query = entityManager.createQuery(consulta, Cliente.class);
            query.setParameter("id", id);
            cliente = query.getSingleResult();
        }catch (NoResultException e){
            cliente = null;
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
        return cliente;
    }
    public static Cliente recuperaClientePorLogin(String login){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String consulta = "select new br.ufpb.ci.marmitaria.model.domain.Cliente(u.id, u.usuario, u.senha, u.telefone, c.nomeUsuario) from usuario u inner join cliente c on u.id = c.id where u.usuario = :login";
        Cliente cliente = null;
        try {
            Query query = entityManager.createQuery(consulta);
            query.setParameter("login", login);
            cliente = (Cliente) query.getSingleResult();
            //System.out.println(query.getSingleResult().toString());
        }catch (NoResultException e){
            cliente = null;
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
        return cliente;
    }

    public static void removeCliente(int idCliente){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String comando1 = "delete from cliente c where c.id = :id";
        String comando2 = "delete from usuario u where u.id = :id";
        try{
            entityManager.getTransaction().begin();
            entityManager.createQuery(comando1).setParameter("id", idCliente).executeUpdate();
            entityManager.createQuery(comando2).setParameter("id", idCliente).executeUpdate();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            if(entityManager.isOpen())
                entityManager.getTransaction().rollback();
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
    }

    public static List listaTodosClientes(){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String consulta = "select u.id, u.telefone, c.nomeUsuario from usuario u inner join cliente c on u.id = c.id";

        TypedQuery<Cliente> query = entityManager.createQuery(consulta, Cliente.class);

        List<Cliente> listaCliente = query.getResultList();

        if(entityManager.isOpen())
            entityManager.close();

        return listaCliente;
    }
}
