package br.ufpb.ci.marmitaria.model.domain;

import com.google.gson.Gson;

import javax.persistence.Entity;

@Entity(name = "cliente")
public class Cliente extends Usuario {
    private String nomeUsuario;

    public Cliente() {
    }

    public Cliente(int id, String usuario, String senha, String telefone, String nomeUsuario) {
        super(id, usuario, senha, telefone);
        this.nomeUsuario = nomeUsuario;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String toJson(){
        return new Gson().toJson(this);
    }
}