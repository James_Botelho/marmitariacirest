package br.ufpb.ci.marmitaria.model.business;

import br.ufpb.ci.marmitaria.controller.seguranca.GerenciaToken;
import br.ufpb.ci.marmitaria.model.dao.ClienteDAO;
import br.ufpb.ci.marmitaria.model.dao.FornecedorDAO;
import br.ufpb.ci.marmitaria.model.domain.Cliente;
import br.ufpb.ci.marmitaria.model.domain.Credencial;
import br.ufpb.ci.marmitaria.model.domain.Fornecedor;
import br.ufpb.ci.marmitaria.model.domain.Login;
import br.ufpb.ci.marmitaria.utils.excecao.LoginIncorretoException;
import com.google.gson.Gson;

import java.security.NoSuchAlgorithmException;

public class GerenciaLogin {

    private static Fornecedor verificaFornecedor(Credencial credencial) throws NoSuchAlgorithmException {
        String senhaCript = Criptografia.criptMD5(credencial.getSenha());
        Fornecedor f = FornecedorDAO.recuperaFornecedorPorLogin(credencial.getLogin());
        if(f != null && f.getSenha().equals(senhaCript))
            return f;
        else
            return null;
    }

    private static Cliente verificaCliente(Credencial credencial) throws NoSuchAlgorithmException {
        String senhaCript = Criptografia.criptMD5(credencial.getSenha());
        Cliente cliente = ClienteDAO.recuperaClientePorLogin(credencial.getLogin());
        if(cliente != null && cliente.getSenha().equals(senhaCript))
            return cliente;
        else
            return null;
    }

    public static String realizaLogin(String jsonRecebido) throws LoginIncorretoException, NoSuchAlgorithmException {
        Gson gson = new Gson();

        Credencial credencial = gson.fromJson(jsonRecebido, Credencial.class);
        Cliente c = verificaCliente(credencial);
        Login login = new Login();
        if(c == null){
            Fornecedor f = verificaFornecedor(credencial);
            if(f == null){
                throw new LoginIncorretoException();
            }else{
                String token = GerenciaToken.geraToken(f.getId());
                f.setSenha(""); //Não envia a senha para o cliente
                login.setToken(token);
                login.setFornecedor(f);
            }
        }else{
            String token = GerenciaToken.geraToken(c.getId());
            c.setSenha(""); //Não envia a senha para o cliente
            login.setToken(token);
            login.setCliente(c);
        }
        return gson.toJson(login);
    }
}
