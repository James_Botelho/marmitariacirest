package br.ufpb.ci.marmitaria.model.dao;

import br.ufpb.ci.marmitaria.model.domain.Cardapio;
import br.ufpb.ci.marmitaria.utils.JpaUtil;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

public class CardapioDAO {

    public static void salvar(Cardapio c){
        EntityManager entityManager = JpaUtil.getEntityManager();
        try{
            entityManager.getTransaction().begin();
            entityManager.persist(c);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            if(entityManager.isOpen())
                entityManager.getTransaction().rollback();
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
    }

    public static Cardapio recuperaCardapioPorId(int id){
        EntityManager entityManager = JpaUtil.getEntityManager();
        Cardapio c;
        try {
            c = entityManager.find(Cardapio.class, id);
        }catch (NoResultException e){
            c = null;
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
        return c;
    }

    public static void removeCardapio(int id){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String comando = "delete from cardapio c where c.id = :id";
        try{
            entityManager.getTransaction().begin();
            entityManager.createQuery(comando).setParameter("id", id).executeUpdate();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            if(entityManager.isOpen())
                entityManager.getTransaction().rollback();
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
    }

    public static void removeTodosCardapiosDoFornecedor(int id_fornecedor){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String comando = "delete from cardapio c where c.idFornecedor = :id";
        try{
            entityManager.getTransaction().begin();
            entityManager.createQuery(comando).setParameter("id", id_fornecedor).executeUpdate();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            if(entityManager.isOpen())
                entityManager.getTransaction().rollback();
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
    }

    public static List listaCardapioDoFornecedorDoDia(int id_fornecedor){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String consulta = "select c from cardapio c where c.idFornecedor = :idFornecedor AND c.data = :data";

        TypedQuery<Cardapio> query = entityManager.createQuery(consulta, Cardapio.class);
        query.setParameter("idFornecedor", id_fornecedor);
        query.setParameter("data", new Date()); //Pega a hora atual do servidor

        List<Cardapio> listaCardapio = query.getResultList();

        if(entityManager.isOpen())
            entityManager.close();

        return listaCardapio;
    }

    public static List listaCardapioGeralDoDia(){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String consulta = "select c from cardapio c where c.data = :data";

        TypedQuery<Cardapio> query = entityManager.createQuery(consulta, Cardapio.class);
        query.setParameter("data", new Date()); //Hora atual do servidor

        List<Cardapio> listaCardapio = query.getResultList();

        if(entityManager.isOpen())
            entityManager.close();

        return listaCardapio;
    }
}
