package br.ufpb.ci.marmitaria.model.business;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Criptografia {
    public static String criptMD5(String senha) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(senha.getBytes(), 0, senha.length());
        return new BigInteger(1, messageDigest.digest()).toString(16);
    }
}
