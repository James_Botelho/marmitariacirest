package br.ufpb.ci.marmitaria.model.dao;

import br.ufpb.ci.marmitaria.model.domain.Cardapio;
import br.ufpb.ci.marmitaria.model.domain.Pedido;
import br.ufpb.ci.marmitaria.utils.JpaUtil;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

public class PedidoDAO {

    public static void inserePedido(Pedido pedido){
        EntityManager entityManager = JpaUtil.getEntityManager();
        try{
            entityManager.getTransaction().begin();
            entityManager.persist(pedido);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            if(entityManager.isOpen())
                entityManager.getTransaction().rollback();
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
    }

    public static void removePedido(int id){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String comando = "delete from pedido p where p.id = :id";
        try{
            entityManager.getTransaction().begin();
            entityManager.createQuery(comando).setParameter("id", id).executeUpdate();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            if(entityManager.isOpen())
                entityManager.getTransaction().rollback();
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
    }

    public static List listaPedidoDoCliente(int idCliente){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String consulta = "select p from pedido p where p.idUsuario = :id and p.data = :data";

        TypedQuery<Pedido> query = entityManager.createQuery(consulta, Pedido.class);
        query.setParameter("id", idCliente);
        query.setParameter("data", new Date());
        List<Pedido> listaPedido = query.getResultList();
        entityManager.close();
        return listaPedido;
    }

    public static List listaPedidoDoFornecedor(int idFornecedor){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String consulta = "select new br.ufpb.ci.marmitaria.model.domain.Pedido(p.id, p.itens, p.idMarmita, p.idUsuario, p.idFornecedor) from pedido p where p.idFornecedor = :id and p.data = :data";

        TypedQuery<Pedido> query = entityManager.createQuery(consulta, Pedido.class);
        query.setParameter("id", idFornecedor);
        query.setParameter("data", new Date());

        List<Pedido> listaPedido = query.getResultList();
        entityManager.close();
        return listaPedido;
    }

    public static Pedido recuperaPedidoPorId(int id){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String consulta = "select p from pedido p where p.idUsuario = :id";

        TypedQuery<Pedido> query = entityManager.createQuery(consulta, Pedido.class);
        query.setParameter("id", id);
        Pedido p = null;
        try{
            p = query.getSingleResult();
        }catch (NoResultException e){}

        entityManager.close();

        return p;
    }
}
