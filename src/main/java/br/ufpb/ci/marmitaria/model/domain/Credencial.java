package br.ufpb.ci.marmitaria.model.domain;

import com.google.gson.Gson;

import java.io.Serializable;

public class Credencial implements Serializable {
    private String login;
    private String senha;

    public Credencial(){}

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String toJson(){
        return new Gson().toJson(this);
    }
}
