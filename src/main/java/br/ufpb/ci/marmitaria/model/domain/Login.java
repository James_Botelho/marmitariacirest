package br.ufpb.ci.marmitaria.model.domain;

import com.google.gson.Gson;

import java.io.Serializable;

public class Login implements Serializable {
    private String token;
    private Fornecedor fornecedor;
    private Cliente cliente;

    public Login(){}

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String toJson(){
        return new Gson().toJson(this);
    }
}
