package br.ufpb.ci.marmitaria.model.business;

public class GerenciaConcorrencia {

    private boolean travaFornecedor;
    private boolean travaCliente;
    private Object travaEntrada;
    private Object travaSaida;

    private static GerenciaConcorrencia instancia;

    private GerenciaConcorrencia(){
        System.out.println("Chamou o construtor");
        travaFornecedor = false;
        travaCliente = false;
        travaEntrada = new Object();
        travaSaida = new Object();
    }

    public static GerenciaConcorrencia recuperaInstancia(){
        if(instancia == null)
            instancia = new GerenciaConcorrencia();
        return instancia;
    }

    public void clientePedePassagem() throws InterruptedException {
        synchronized (this){
            while(travaCliente) {
                wait();
            }
            travaFornecedor = true;
        }
    }

    public void fornecedorPedePassagem() throws InterruptedException {
        synchronized (this){
            while(travaFornecedor){
                wait();
            }
            travaCliente = true;
        }
    }

    public void clienteLiberaPassagemParaFornecedor(){
        synchronized (this){
            travaFornecedor = false;
            notifyAll();
        }
    }

    public void fornecedorLiberaPassagemParaCliente(){
        synchronized (this){
            travaCliente = false;
            notifyAll();
        }
    }
}
