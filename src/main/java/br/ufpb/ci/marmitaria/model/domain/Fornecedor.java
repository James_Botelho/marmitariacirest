package br.ufpb.ci.marmitaria.model.domain;

import com.google.gson.Gson;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name = "fornecedor")
public class Fornecedor extends Usuario {
    private String nomeEmpresa;
    private String endereco;

    public Fornecedor(int id, String usuario, String senha, String telefone, String nomeEmpresa, String endereco) {
        super(id, usuario, senha, telefone);
        this.nomeEmpresa = nomeEmpresa;
        this.endereco = endereco;
    }

    public Fornecedor() {
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String toJson(){
        return new Gson().toJson(this);
    }
}
