package br.ufpb.ci.marmitaria.model.dao;

import br.ufpb.ci.marmitaria.model.domain.Cliente;
import br.ufpb.ci.marmitaria.model.domain.Fornecedor;
import br.ufpb.ci.marmitaria.utils.JpaUtil;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class FornecedorDAO {

    public static void salvaFornecedor(Fornecedor fornecedor){
        EntityManager entityManager = JpaUtil.getEntityManager();
        try{
            entityManager.getTransaction().begin();
            entityManager.persist(fornecedor);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            if(entityManager.isOpen())
                entityManager.getTransaction().rollback();
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
    }

    public static void atualizaFornecedor(Fornecedor fornecedor){
        EntityManager entityManager = JpaUtil.getEntityManager();
        try{
            entityManager.getTransaction().begin();
            entityManager.merge(fornecedor);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            if(entityManager.isOpen())
                entityManager.getTransaction().rollback();
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
    }

    public static Fornecedor recuperaFornecedorPorId(int id){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String consulta = "select new br.ufpb.ci.marmitaria.model.domain.Fornecedor(u.id, u.usuario, u.senha, f.telefone, f.nomeEmpresa, f.endereco) from usuario u inner join fornecedor f on u.id = f.id where u.id = :id";
        Fornecedor fornecedor;
        try {
            TypedQuery<Fornecedor> query = entityManager.createQuery(consulta, Fornecedor.class);
            query.setParameter("id", id);
            fornecedor = query.getSingleResult();
        }catch (NoResultException e){
            fornecedor = null;
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
        return fornecedor;
    }
    public static Fornecedor recuperaFornecedorPorLogin(String login){
        EntityManager entityManager = JpaUtil.getEntityManager();

        String consulta = "select new br.ufpb.ci.marmitaria.model.domain.Fornecedor(u.id, u.usuario, u.senha, f.telefone, f.nomeEmpresa, f.endereco) from usuario u inner join fornecedor f on u.id = f.id where u.usuario = :usuario";
        Fornecedor fornecedor = null;
        try {
            TypedQuery<Fornecedor> query = entityManager.createQuery(consulta, Fornecedor.class);
            query.setParameter("usuario", login);
            fornecedor = query.getSingleResult();
            //System.out.println(query.getSingleResult().getClass());
        }catch (NoResultException e){
            fornecedor = null;
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
        return fornecedor;
    }

    public static void removeFornecedor(int idFornecedor){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String comando = "delete from usuario u where u.id = :id";
        try{
            entityManager.getTransaction().begin();
            entityManager.createQuery(comando).setParameter("id", idFornecedor).executeUpdate();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            if(entityManager.isOpen())
                entityManager.getTransaction().rollback();
        }finally {
            if(entityManager.isOpen())
                entityManager.close();
        }
    }

    public static List listaTodosFornecedores(){
        EntityManager entityManager = JpaUtil.getEntityManager();
        String comando = "select new br.ufpb.ci.marmitaria.model.domain.Fornecedor(u.id, u.usuario, u.senha, f.telefone, f.nomeEmpresa, f.endereco) from usuario u inner join fornecedor f on u.id = f.id";

        TypedQuery<Fornecedor> query = entityManager.createQuery(comando, Fornecedor.class);

        List<Fornecedor> listaFornecedor = query.getResultList();

        if(entityManager.isOpen())
            entityManager.close();

        return listaFornecedor;
    }
}
