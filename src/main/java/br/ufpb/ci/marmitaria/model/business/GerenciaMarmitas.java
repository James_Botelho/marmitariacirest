package br.ufpb.ci.marmitaria.model.business;

import br.ufpb.ci.marmitaria.model.dao.CardapioDAO;
import br.ufpb.ci.marmitaria.model.dao.ClienteDAO;
import br.ufpb.ci.marmitaria.model.dao.FornecedorDAO;
import br.ufpb.ci.marmitaria.model.domain.Cardapio;
import br.ufpb.ci.marmitaria.model.domain.Cliente;
import br.ufpb.ci.marmitaria.model.domain.Fornecedor;
import br.ufpb.ci.marmitaria.utils.excecao.SemAutorizacaoException;
import com.google.gson.Gson;

import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

public class GerenciaMarmitas {

    public static void adicionaMarmita(String conteudo, String tokenId) throws SemAutorizacaoException {
        int idTk = Integer.parseInt(tokenId);
        Gson gson = new Gson();
        Cardapio cardapio = gson.fromJson(conteudo, Cardapio.class);
        StringTokenizer stringTokenizer = new StringTokenizer(cardapio.getItens(), ",");
        cardapio.setQtdItens(stringTokenizer.countTokens());
        cardapio.setData(new Date());
        Fornecedor fornecedor = FornecedorDAO.recuperaFornecedorPorId(idTk);
        if(fornecedor == null || cardapio.getIdFornecedor() != idTk){
            throw new SemAutorizacaoException();
        }
        CardapioDAO.salvar(cardapio);
    }

    //Apenas cliente pode listar cardapio geral
    public static String listaTodosCardapios(String tokenId) throws SemAutorizacaoException {
        int idTk = Integer.parseInt(tokenId);
        Cliente c = ClienteDAO.recuperaClientePorId(idTk);
        if(c == null){
            throw new SemAutorizacaoException();
        }
        Gson gson = new Gson();
        List cardapioLista = CardapioDAO.listaCardapioGeralDoDia();
        return gson.toJson(cardapioLista);
    }

    //Apenas fornecedor pode listar seu proprio cardapio
    public static String listaCardapioDoFornecedor(int idSolicitado, String tokenId) throws SemAutorizacaoException {
        Integer idTk = Integer.parseInt(tokenId);
        Fornecedor fornecedor = FornecedorDAO.recuperaFornecedorPorId(idTk);
        if(fornecedor == null || idSolicitado != idTk){
            throw new SemAutorizacaoException();
        }
        Gson gson = new Gson();
        List cardapioLista = CardapioDAO.listaCardapioDoFornecedorDoDia(idTk);
        return gson.toJson(cardapioLista);
    }

    public static void removeMarmita(int idCardapio, String tokenId) throws SemAutorizacaoException {
        Integer idTk = Integer.parseInt(tokenId);
        Fornecedor fornecedor = FornecedorDAO.recuperaFornecedorPorId(idTk);
        if(fornecedor == null){
            throw new SemAutorizacaoException();
        }
        Cardapio cardapio = CardapioDAO.recuperaCardapioPorId(idCardapio);
        if(cardapio.getIdFornecedor() != idTk){
            throw new SemAutorizacaoException();
        }
        CardapioDAO.removeCardapio(idCardapio);
    }
}
