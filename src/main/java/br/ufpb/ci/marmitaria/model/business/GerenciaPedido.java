package br.ufpb.ci.marmitaria.model.business;

import br.ufpb.ci.marmitaria.model.dao.ClienteDAO;
import br.ufpb.ci.marmitaria.model.dao.FornecedorDAO;
import br.ufpb.ci.marmitaria.model.dao.PedidoDAO;
import br.ufpb.ci.marmitaria.model.domain.Cliente;
import br.ufpb.ci.marmitaria.model.domain.Fornecedor;
import br.ufpb.ci.marmitaria.model.domain.Pedido;
import br.ufpb.ci.marmitaria.utils.excecao.SemAutorizacaoException;
import com.google.gson.Gson;

import java.util.Date;
import java.util.List;

public class GerenciaPedido {

    public static void adicionaPedido(String conteudo, String tokenId) throws SemAutorizacaoException {
        int idTk = Integer.parseInt(tokenId);
        Cliente cliente = ClienteDAO.recuperaClientePorId(idTk);
        if(cliente == null)
            throw new SemAutorizacaoException();
        Gson gson = new Gson();
        Pedido pedido = gson.fromJson(conteudo, Pedido.class);

        if(pedido.getIdUsuario() != idTk)
            throw new SemAutorizacaoException();

        pedido.setData(new Date());
        PedidoDAO.inserePedido(pedido);
    }

    public static String listaPedido(String tokenId) throws SemAutorizacaoException {
        int idTk = Integer.parseInt(tokenId);
        Cliente cliente = ClienteDAO.recuperaClientePorId(idTk);
        Fornecedor fornecedor = FornecedorDAO.recuperaFornecedorPorId(idTk);
        if(cliente == null && fornecedor == null){
            throw new SemAutorizacaoException();
        }
        List listaPedidos;
        if(cliente != null)
            listaPedidos = PedidoDAO.listaPedidoDoCliente(idTk);
        else
            listaPedidos = PedidoDAO.listaPedidoDoFornecedor(idTk);

        Gson gson = new Gson();

        String conteudo = gson.toJson(listaPedidos);
        System.out.println(conteudo);
        return conteudo;
    }

    public static void deletaPedido(int id, String tokenId) throws SemAutorizacaoException {
        int idTk = Integer.parseInt(tokenId);
        Cliente cliente = ClienteDAO.recuperaClientePorId(idTk);

        if(cliente == null)
            throw new SemAutorizacaoException();

        Pedido p = PedidoDAO.recuperaPedidoPorId(id);

        if(p.getIdUsuario() != id)
            throw new SemAutorizacaoException();

        PedidoDAO.removePedido(id);
    }
}
