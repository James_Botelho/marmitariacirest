package br.ufpb.ci.marmitaria.model.business;

import br.ufpb.ci.marmitaria.model.dao.CardapioDAO;
import br.ufpb.ci.marmitaria.model.dao.ClienteDAO;
import br.ufpb.ci.marmitaria.model.dao.FornecedorDAO;
import br.ufpb.ci.marmitaria.model.domain.Cliente;
import br.ufpb.ci.marmitaria.model.domain.Fornecedor;
import br.ufpb.ci.marmitaria.model.domain.Usuario;
import br.ufpb.ci.marmitaria.utils.excecao.LoginExistenteException;
import br.ufpb.ci.marmitaria.utils.excecao.SemAutorizacaoException;
import com.google.gson.Gson;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public class GerenciaUsuario {

    private static synchronized void insereUsuario(Usuario u) throws LoginExistenteException, NoSuchAlgorithmException {
        boolean ehFornecedor = u instanceof Fornecedor;

        Usuario usuario = u;
        usuario.setSenha(Criptografia.criptMD5(u.getSenha()));

        if(FornecedorDAO.recuperaFornecedorPorLogin(u.getUsuario()) == null && ClienteDAO.recuperaClientePorLogin(u.getUsuario()) == null){
            if(ehFornecedor){
                FornecedorDAO.salvaFornecedor((Fornecedor)usuario);
            }else{
                ClienteDAO.salvaCliente((Cliente)usuario);
            }
        } else {
            throw new LoginExistenteException();
        }
    }

    public static void insereFornecedor(String jsonRecebido) throws LoginExistenteException, NoSuchAlgorithmException {
        Gson gson = new Gson();
        Fornecedor f = gson.fromJson(jsonRecebido, Fornecedor.class);
        insereUsuario(f);
    }

    public static void insereCliente(String jsonRecebido) throws LoginExistenteException, NoSuchAlgorithmException {
        Gson gson = new Gson();
        Cliente c = gson.fromJson(jsonRecebido, Cliente.class);
        insereUsuario(c);
    }

    public static void atualizaFornecedor(String jsonRecebido, String idToken) throws SemAutorizacaoException {
        Integer idTk = Integer.parseInt(idToken);
        Gson gson = new Gson();
        Fornecedor fornecedor = gson.fromJson(jsonRecebido, Fornecedor.class);
        if(fornecedor.getId() != idTk){
            throw new SemAutorizacaoException();
        }
        FornecedorDAO.atualizaFornecedor(fornecedor);
    }

    public static void atualizaCliente(String jsonRecebido, String idToken) throws SemAutorizacaoException {
        Integer idTk = Integer.parseInt(idToken);
        Gson gson = new Gson();
        Cliente cliente = gson.fromJson(jsonRecebido, Cliente.class);
        if(cliente.getId() != idTk){
            throw new SemAutorizacaoException();
        }
        ClienteDAO.atualizaCliente(cliente);
    }

    public static void removeCliente(int idSolicitado, String idToken) throws SemAutorizacaoException {
        Integer idTk = Integer.parseInt(idToken);
        if (idSolicitado != idTk){
            throw new SemAutorizacaoException();
        }

        ClienteDAO.removeCliente(idSolicitado);
    }

    public static void removeFornecedor(int idSolicitado, String idToken) throws SemAutorizacaoException {
        Integer idTk = Integer.parseInt(idToken);
        if(idSolicitado != idTk){
            throw new SemAutorizacaoException();
        }
        //Remove cardápios antes por motivo de relacionamento
        CardapioDAO.removeTodosCardapiosDoFornecedor(idSolicitado);
        FornecedorDAO.removeFornecedor(idSolicitado);
    }

    public static String listaTodosFornecedores(String idToken) throws SemAutorizacaoException {
        Integer idTk = Integer.parseInt(idToken);
        Cliente cliente = ClienteDAO.recuperaClientePorId(idTk);
        if(cliente == null){
            throw new SemAutorizacaoException();
        }
        Gson gson = new Gson();
        List fornecedorList = FornecedorDAO.listaTodosFornecedores();

        return gson.toJson(fornecedorList);
    }
}
