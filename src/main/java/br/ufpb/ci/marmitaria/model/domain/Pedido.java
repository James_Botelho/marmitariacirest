package br.ufpb.ci.marmitaria.model.domain;

import com.google.gson.Gson;
import org.hibernate.type.DateType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "pedido")
public class Pedido implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String itens;
    private int idMarmita;
    private int idUsuario;
    private int idFornecedor;
    @Temporal(TemporalType.DATE)
    private Date data;

    public Pedido(int id, String itens, int idMarmita, int idUsuario, int idFornecedor) {
        this.id = id;
        this.itens = itens;
        this.idMarmita = idMarmita;
        this.idUsuario = idUsuario;
        this.idFornecedor = idFornecedor;
    }

    public Pedido(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItens() {
        return itens;
    }

    public void setItens(String itens) {
        this.itens = itens;
    }

    public int getIdMarmita() {
        return idMarmita;
    }

    public void setIdMarmita(int idMarmita) {
        this.idMarmita = idMarmita;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String toJson(){
        return new Gson().toJson(this);
    }
}
