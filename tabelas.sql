DROP DATABASE IF EXISTS marmitariaci;
CREATE DATABASE marmitariaci;

DROP TABLE IF EXISTS usuario;
DROP TABLE IF EXISTS fornecedor;
DROP TABLE IF EXISTS cliente;
DROP TABLE IF EXISTS cardapio;
DROP TABLE IF EXISTS pedido;

CREATE TABLE usuario (
    id int NOT NULL AUTO_INCREMENT,
    usuario varchar(32) NOT NULL UNIQUE,
    senha varchar(100) NOT NULL,
    telefone VARCHAR(11) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE fornecedor (
    id int NOT NULL UNIQUE,
    nomeEmpresa VARCHAR(255) NOT NULL,
    endereco VARCHAR(255) NOT NULL,
    FOREIGN KEY (id) REFERENCES usuario(id)
);

CREATE TABLE cliente (
    id int NOT NULL UNIQUE,
    nomeUsuario VARCHAR(255) NOT NULL,
    FOREIGN KEY (id) REFERENCES usuario(id)
);

CREATE TABLE cardapio (
    id int NOT NULL AUTO_INCREMENT,
    itens varchar(32) NOT NULL,
    qtdItens int NOT NULL,
    data DATE NOT NULL,
    preco varchar(32) NOT NULL,
    idFornecedor int NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (idFornecedor) REFERENCES usuario(id)
);

CREATE TABLE pedido(
    id int NOT NULL AUTO_INCREMENT,
    idMarmita int NOT NULL,
    idUsuario int NOT NULL,
    idFornecedor int NOT NULL,
    itens VARCHAR(255) NOT NULL,
    data DATE NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (idMarmita) REFERENCES cardapio(id),
    FOREIGN KEY (idUsuario) REFERENCES usuario(id),
    FOREIGN KEY (idFornecedor) REFERENCES usuario(id)
);